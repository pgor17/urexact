
# urexact

Exact Solution from Unrooted Gene Trees

---

## Usage

Input options:

>  -s FILE/TREE - define species tree(s); optional, used for testing correctness  
>  -g FILE/TREE - define gene tree(s)    

Reconciliation:  

>  -c D|L|M|C|2 - cost spec: D=Dup (default), L=Loss, M=Dup+Loss, C=Deep Coalescence, 2=DC-classic  
>  -e - smp reconciliation (cost computation naive vs. star/cluster based)

---

## Typical run for unrooted gene tree input

Only dynamic programming (DP); Dup cost (default). 
The script prints the cost summary and the list of the species trees with the minimal cost.

### ./urexact.py  -g gtrees.txt  

Min urD cost (DP): 0  
(((a,c),b),d)  
(((a,b),c),d)  

---

## Unrooted Loss Cost by DP.

### ./urexact.py -cL -g gtrees.txt  

Min urL cost (DP): 1  
(a,(b,(c,d)))  
((a,b),(c,d))  
(((a,c),b),d)  
((a,(c,d)),b)  
(((a,b),d),c)  

---

## Testing correctness of DP

Provide the set of all rooted species trees over N leaves (e.g., 3.txt, 4.txt. etc.)

### ./urexact.py -g '(a,(b,(c,c)))' -s 3.txt

Min D cost (naive):  1
(c,(a,b))
(b,(a,c))
(a,(b,c))
Min urD cost (DP): 1
(a,(b,c))
((a,c),b)
((a,b),c)

---

## Testing correctness of unrooted cost computation 

c(G,S) formula vs naive method

### ./urexact.py -cL -e -g "(((c,a),(a,c)),(a,d))" -s 4.txt

Testing correcntess: G vs fixed S  
G=(((c,a),(a,c)),(a,d)) S=(d,(c,(a,b)))  L=4 OK  
G=(((c,a),(a,c)),(a,d)) S=(d,(b,(a,c)))  L=2 OK  
G=(((c,a),(a,c)),(a,d)) S=(d,(a,(b,c)))  L=3 OK  
G=(((c,a),(a,c)),(a,d)) S=(a,(d,(b,c)))  L=5 OK  
G=(((c,a),(a,c)),(a,d)) S=(b,(d,(a,c)))  L=1 OK  
G=(((c,a),(a,c)),(a,d)) S=(c,(d,(a,b)))  L=6 OK  
G=(((c,a),(a,c)),(a,d)) S=(c,(b,(a,d)))  L=6 OK  
G=(((c,a),(a,c)),(a,d)) S=(c,(a,(b,d)))  L=4 OK  
G=(((c,a),(a,c)),(a,d)) S=((a,c),(b,d))  L=2 OK  
G=(((c,a),(a,c)),(a,d)) S=(a,(c,(b,d)))  L=4 OK  
G=(((c,a),(a,c)),(a,d)) S=((b,c),(a,d))  L=5 OK  
G=(((c,a),(a,c)),(a,d)) S=(b,(c,(a,d)))  L=3 OK  
G=(((c,a),(a,c)),(a,d)) S=(b,(a,(c,d)))  L=3 OK  
G=(((c,a),(a,c)),(a,d)) S=((a,b),(c,d))  L=6 OK  
G=(((c,a),(a,c)),(a,d)) S=(a,(b,(c,d)))  L=6 OK  
  
---

## The contents of the example file:

### cat gtrees.txt

(a,((b,b),c))  
((a,b),(c,d))  




