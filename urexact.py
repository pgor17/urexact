#!/usr/bin/python

import sys 
from treeop import Tree, str2tree, node2label, randtree, ns, pt, startype
import getopt,itertools

verbose=""
OPTDP=1
OPTSINGLE=2

def pc(c): return "".join(sorted(c))

def readgsfile(filename):

    # First 

    try:
        t=[ l.strip() for l in open(filename,"r").readlines() if len(l.strip()) and l.strip()[0]!='#' ]
    except IOError as e:
        print filename," I/O error({0}): {1}".format(e.errno, e.strerror)
        quit() 
    
    if t[0][0].isdigit():
        gtnum=int(t[0])
        offset=1
    else: 
        gtnum=1
        offset=0
    
    gtrees=[] 
 
    for i in xrange(gtnum):
        if verbose.find("0") <> -1:
            print "Processing line ", t[i+offset]
        gtrees.append(Tree(str2tree(t[i+offset])))
    st=Tree(str2tree(t[i+1+offset]))

    stnodespostorder=st.root.nodes()

    oldgtnum=-1
   
    for i in xrange(i+2+offset,len(t)):
    
        gtnum,gc,sc,l=t[i].replace("+"," +").split(";")
        l=int(l)
        gtnum=int(gtnum)

        if oldgtnum!=gtnum:
            gt=gtrees[gtnum]
            gtnodespostorder=gt.root.nodes()
            oldgtnum=gtnum
        
        if gc[0].isdigit():
            g=gtnodespostorder[int(gc)]
            s=stnodespostorder[int(sc)]
            l=stnodespostorder[l]

        else:
            gc=gc.strip().split()
            sc=sc.strip().split()


            g=gt.findnodeplus(gc)
            s=st.findnodeplus(sc)


        if not g:
            raise Exception("Incorrect interval cluster "+str(gc))
        if not s:
            raise Exception("Incorrect interval cluster "+str(sc))

        g.setinterval(s,l)

        
    return gtrees,st


def readgs(filename):

    t=[ l.strip() for l in open(filename,"r").readlines() if len(l.strip()) and l.strip()[0]!='#' ]
    if verbose.find("0") <> -1:
        print t
    #print str2tree(t[0])
    return Tree(str2tree(t[0])),Tree(str2tree(t[1]))



def readgtreefile(gtreefile):
    gtrees=[]
    for g in open(gtreefile).readlines():
         if g and g[0]!="#":
            gtrees.append(Tree(str2tree(g.strip())))

    return gtrees


class Cost:
    def gammaf(self,a1,a2,x1,x2): return 0
    def lambdaf(self,a1,a2,s): return 0

    def ulambdaf(self,a,b,c,s,topsplit): return 0
    def ugammaf(self,a,b,c,x,y,topsplit): 
        if x==topsplit[0] or x==topsplit[1]:
            return self.ugammatopf(a,b,c,topsplit)
        return self.ugammaintf(a,b,c,x,y,topsplit)

    def ugammaintf(self,a,b,c,x,y,topsplit): 
        t,a,b,c=startype(a,b,c,topsplit)
        #if t==1 or t==2: 
        return self.gammaf(b,c,x,y) 
        #return 0

    def ugammatopf(self,a,b,c,topsplit): return 0

    def naiveunrooted(self,u,s): 
        lst=[ (self.smprooted(Tree(g),s),g) for g in u.allrootings() ]
        mc=min(c for c,_ in lst)
        for c,mt in lst: 
            if c==mc: return mt,mc        

    def naiveunrootedbygencost(self,u,s): 
        lst=[ (Tree(g).gencost(s,self),g) for g in u.allrootings() ]
        mc=min(c for c,_ in lst)
        for c,mt in lst: 
            if c==mc: return mt,mc      

    def optname(self): return self.name()    
    def name(self): return "Null"
    def urtreecorrection(self,gtreecnt): return 0
    def rtreecorrection(self,gtrees): return 0


class CostDup(Cost):   
    def gammaf(self,a1,a2,x1,x2):
        r=a1.union(a2).issubset(x1.union(x2))
        s=not a1.issubset(x1) and not a1.issubset(x2) or not a2.issubset(x1) and not a2.issubset(x2)
        return int(r and s)    

    def ugammatopf(self,a,b,c,topsplit): 
        t,a,b,c=startype(a,b,c,topsplit)
        X,Y=topsplit
        rc=self.gammaf(b,c,X,Y) 
        rc=0
        if t==2:
            if len(a)==1: return rc-1 # one star S2 / or leaf dupl                
            if len(b.union(c))>1: return rc-0.5  # two stars S2; two such cases 
            return 0 # 1 dupl; |B cup C|=1
            #return rc-0.5 # two stars S2
        if t==4 or t==5: return 1
        return 0

    def ulambdaf(self,a,b,c,s,topsplit):
        t,a,b,c=startype(a,b,c,topsplit)        
        return int(t in (1,2) and b.union(c)==frozenset(s))

    def lambdaf(self,a1,a2,s): return int(a1.union(a2)==s)    
            
    def name(self): return "D"    
    def smprooted(self,g,s): return g.dupcost(s)
    def urtreecorrection(self,gtrees): return len(gtrees)


# Madisson DC
# No unrooted
class CostDC2(Cost):   
    def optname(self): return "2"
    def name(self): return "DC2"
    def smprooted(self,g,s): return g.dccost(s)
    def lambdaf(self,a1,a2,s):        
        return -2*int(a1.union(a2)==s)  
    
    def gammaf(self,a1,a2,x1,x2):
        xs=x1.union(x2)
        asm=a1.union(a2)    
        r=a1.union(a2).issubset(x1.union(x2))
        s=not a1.issubset(x1) and not a1.issubset(x2) or not a2.issubset(x1) and not a2.issubset(x2)
        l=2*int(r and s)  

        for A1,A2 in ((a1,a2),(a2,a1)):
            for X1,X2 in ((x1,x2),(x2,x1)):                                        
                if A1.issubset(X1) and not A2.issubset(X2) and not A2.issubset(X1):  
                    return 1-l                 
        return -l

# Edge counting DC 
class CostDC(Cost):   
    def optname(self): return "C"
    def name(self): return "DC"
    def smprooted(self,g,s): return g.dccost2(s)
    def lambdaf(self,a1,a2,s): return int(a1!=a2 and (s==a1 or s==a2))
    
    def ugammaf2(self,a,b,c,x,y,topsplit): 
        t,a,b,c=startype(a,b,c,topsplit)
        X,Y=topsplit
        #print "GInt %s.%s.%s %s|%s",t,ns(a),ns(b),ns(c),ns(x),ns(y)
        rc=self.gammaf(b,c,x,y) 
        if X==x or X==y:
            #top            
            #print "top",rc,ns(b,c,x,y)
            if t==2: 
                if b==c and len(b)==1: return 0
                return rc
            if t==4 or t==5: return rc #???
            return 0
        else:
            #print "int",rc,ns(b,c,x,y)
            if t==2: 
                if b==c and len(b)==1: return 0
                return rc+self.gammaf(b.union(c),a,x,y)*(0.5 if len(a)>1 else 1)
            if t==1: return rc#+self.gammaf(b.union(c),a,x,y)
            if t==3: return self.gammaf(b,c,x,y)
            if t==5: return self.gammaf(b.union(c),a,x,y)
            return 0

    def ugammatopf(self,a,b,c,topsplit): return 0

    def ugammaintf(self,a,b,c,x,y,topsplit): 
        t,a,b,c=startype(a,b,c,topsplit)
        rc=self.gammaf(b,c,x,y) 
        ac=self.gammaf(b.union(c),a,x,y)
        if t==2: 
            if b==c and len(b)==1: return 0 # NEEDED
            return rc+ac*(0.5 if len(a)>1 else 1)
            
        if t==1 or t==3: return rc #OK
        if t==5: return ac #OK
        return 0

    

    def ulambdaf(self,a,b,c,s,topsplit):
        t,a,b,c=startype(a,b,c,topsplit)
        rc=self.lambdaf(b,c,frozenset(s))
        ac=self.lambdaf(b.union(c),a,frozenset(s))
        #f b==c and len(b)==1: return 0 # ignore duplicated leaves        
        if t==2: 
            if b==c and len(b)==1: return 0
            return int(rc or ac) # both needed            
        if t==5: return ac # OK                    
        if t==1 or t==3: return rc # OK            
        return 0

    def gammaf(self,a1,a2,x1,x2):
        xs=x1.union(x2)
        asm=a1.union(a2)
        for A1,A2 in ((a1,a2),(a2,a1)):
            for X1,X2 in ((x1,x2),(x2,x1)):
                if A1.issubset(xs) and not A1.issubset(X2) and not A2.issubset(xs):                                 
                    return 1         
        return 0

class CostW(Cost):
    def __init__(self,weigth):         
        self.dupweight=weigth
        self.dup=CostDup()
        self.dc=CostDC()

    def name(self): return "W"

    def gammaf(self,a1,a2,x1,x2):
        return self.dupweight*self.dup.gammaf(a1,a2,x1,x2)+self.dc.gammaf(a1,a2,x1,x2)        
    def lambdaf(self,a1,a2,s):
        return self.dupweight*self.dup.lambdaf(a1,a2,s)+self.dc.lambdaf(a1,a2,s)        

    def ulambdaf(self,a,b,c,s,topsplit): 
        return self.dupweight*self.dup.ulambdaf(a,b,c,s,topsplit)+self.dc.ulambdaf(a,b,c,s,topsplit)
    def ugammaintf(self,a,b,c,x,y,topsplit): 
        return self.dupweight*self.dup.ugammaintf(a,b,c,x,y,topsplit)+self.dc.ugammaintf(a,b,c,x,y,topsplit)    
    def ugammatopf(self,a,b,c,topsplit):         
        return self.dupweight*self.dup.ugammatopf(a,b,c,topsplit)+self.dc.ugammatopf(a,b,c,topsplit)    

    def urtreecorrection(self,gtrees): 
        #DC CORRECTION: DC'=DC-(2n-2)-unrooted D correction
        return -sum(2*len(g.leaves())-2-self.dupweight for g in gtrees)    

    def rtreecorrection(self,gtrees): 
        #DC CORRECTION: DC'=DC-(2n-2)
        #return 0
        return -sum(2*len(g.leaves())-2 for g in gtrees)  


class CostL(CostW):
    def __init__(self): CostW.__init__(self,2)
    def smprooted(self,g,s): return g.losscost(s)
    def name(self): return "L"

class CostDL(CostW):
    def __init__(self): CostW.__init__(self,3)
    def smprooted(self,g,s): return g.duplosscost(s)
    def name(self): return "DL"
    def optname(self): return "M"



def ftread(a):
    try:
        return [ Tree(str2tree(l)) for l in open(a,'r') if l.strip() and l.strip()[0]!='#']
    except Exception as e:
        return [ Tree(str2tree(a))]        \
    

def powerset(iterable):    
    xs = list(iterable)    
    return itertools.chain.from_iterable(itertools.combinations(xs,n) for n in range(len(xs)+1))

def splitset(Z):
    if Z:
        a=min(Z)
        Z=frozenset(Z).difference([a])    
        lz=len(Z)
        for X in powerset(Z):
            if len(X)<lz: 
                yield frozenset(X).union([a]),Z.difference(X)


def splitset2(Z):
    for X in powerset(Z):        
        for s in splitset(X): yield s        

def optTrees(Delta,DpMins,Z):
    if len(Z)==1: yield list(Z)[0]
    else:        
        for X,Y in DpMins[Z]:
            for a,b in itertools.product(optTrees(Delta,DpMins,X),optTrees(Delta,DpMins,Y)):
                yield (a,b)


def uroptTrees(Delta,DpMins,Z,tp=None):
    if len(Z)==1: yield list(Z)[0]
    else:        
        for X,Y,tpx in DpMins[Z,tp]:
            for a,b in itertools.product(uroptTrees(Delta,DpMins,X,tpx),uroptTrees(Delta,DpMins,Y,tpx)):
                yield (a,b)


def processunrootedtrees(gtrees,strees,cost,runopt):
        
    leaves=frozenset([ l.src for g in gtrees for l in g.leaves() ])

    for g in gtrees:
        if len(g.root.cluster)<3: 
            raise Exception("Gene trees with at most two labels are not allowed")

    if runopt==OPTSINGLE:
        # Testing correctess 2
        print "Testing correcntess: G vs fixed S"
        for gt in gtrees:
            for st in strees:                 
                c=gt.ugencost(st,cost,verbose)        
                print "G=%s S=%s  %s=%d"%(gt,st,cost.name(),c),                
                
                ls=cost.naiveunrootedbygencost(gt,st)
                if c!=ls[1]: 
                    print 
                    print "ERROR",
                    print "Expected value:",ls[1]                
                    sys.exit(-1)
                else: print "OK"
        return 

    if runopt==OPTDP:
    
        # Compute best trees by naive method    
        minst=None
        if strees:    
            minc=-1
            for st in strees:
                c=sum(cost.naiveunrooted(gt,st)[1] for gt in gtrees)        
                if minc==-1 or c<minc: minst,minc=[],c
                if minc==c: minst.append(st)

            print 'Min %s cost (naive): '%cost.name(),minc
            for s in minst: print s

        #uLambda - Thm 1.7
        Lambda={}
        for topsplit in splitset(leaves):
            for x in leaves: 
                x=frozenset(x)            
                Lambda[x,topsplit]=sum(cost.ulambdaf(a,b,c,x,topsplit) for g in gtrees for a,b,c in g.stars() ) 

        if 'd' in verbose:
            print "Lambda"
            for x,topsplit in sorted(Lambda):
                print "  %s %s^%s %g"%(ns(x),ns(topsplit[0]),ns(topsplit[1]),Lambda[x,topsplit])

        n=len(leaves)
        Gamma={}
        for TX,TY in splitset(leaves):    
            topsplit=TX,TY
            Gamma[TX,TY,topsplit]=sum(cost.ugammatopf(a,b,c,topsplit) for g in gtrees for a,b,c in g.stars() ) 
            
            for Z in TX,TY:
                for X,Y in splitset2(Z):                                                                                                        
                    Gamma[X,Y,topsplit]=sum(cost.ugammaintf(a,b,c,X,Y,topsplit) for g in gtrees for a,b,c in g.stars() ) 
                    #print "    ",ns(X,Y)
                    if 'd' in verbose:                        
                        print ">>",pc(X)+"|"+pc(Y),Gamma[X,Y,topsplit]
                        for g in gtrees:
                            for a,b in g.splits():
                                print "  ",g,pc(a)+"|"+pc(b),cost.gammaf(a,b,X,Y)


        if 'd' in verbose:
            print "Gamma"
            for X,Y,topsplit in sorted(Gamma):
                print "  %s|%s %s^%s %g"%(ns(X),ns(Y),ns(topsplit[0]),ns(topsplit[1]),Gamma[X,Y,topsplit])
                

        Delta={}
        for topsplit in splitset(leaves):
            for l in leaves:
                Z=frozenset(l)
                Delta[Z,topsplit]=Lambda[Z,topsplit]

        if 'x' in verbose:
            for TX,TY in splitset(leaves):    
                topsplit=TX,TY
                tsp="%s^%s"%(ns(topsplit[0]),ns(topsplit[1]))
                print "TOPSPLIT",tsp
                for g in gtrees:
                    for a,b,c in g.stars():
                        star=pc(a)+"|"+pc(b)+"|"+pc(c)
                        print "  si(%s,T)=%d"%(star,cost.ugammatopf(a,b,c,topsplit))
                        for Z in TX,TY:
                            for X,Y in splitset2(Z):                                                                                                        
                                xy=pc(X)+"|"+pc(Y)
                                print "  ga(%s,%s,T)=%d"%(star,xy,cost.ugammaf(a,b,c,X,Y,topsplit))
                        for x in leaves:
                            x=frozenset(x)
                            xl=pc(x)
                            print "  la(%s,%s,T)=%d"%(star,xl,cost.ulambdaf(a,b,c,x,topsplit))

        
        DpMins={}

        def compDelta(Z,topsplit):        
            if (Z,topsplit) in Delta: return Delta[Z,topsplit]
            settopsplit=not topsplit
            first=1
            dpmins=[]            
            srctopsplit=topsplit
            for X,Y in splitset(Z):            
                if settopsplit: topsplit=(X,Y)
                
                cd1=compDelta(X,topsplit)
                cd2=compDelta(Y,topsplit)            
                cst=cd1+cd2+Gamma[X,Y,topsplit]
                
                if first or cst<dpminc: 
                    dpminc=cst
                    dpmins=[]
                    first=0
                if cst==dpminc:
                    dpmins.append((X,Y,topsplit))
            if not srctopsplit: dpminc+=cost.urtreecorrection(gtrees)
            Delta[Z,srctopsplit]=dpminc
            DpMins[Z,srctopsplit]=dpmins
            return dpminc

        dpminc=compDelta(leaves,None)   

        if 'd' in verbose:
            print 'DELTA'
            for Z,topsplit in Delta:
                if topsplit!=None:
                    print "   %s %s^%s %g"%(ns(Z),ns(topsplit[0]),ns(topsplit[1]),Delta[Z,topsplit])
                else:
                    print "   %s None %g"%(ns(Z),Delta[Z,topsplit])

            for k in DpMins[leaves,None]:
                print k

        print "Min ur%s cost (DP):"%cost.name(),dpminc
        cnt=0
        for t in uroptTrees(Delta,DpMins,leaves):
            print pt(t)
            cnt+=1

        if minst:
            if cnt!=len(minst): 
                print "Error urOpt Size: ",cnt,len(minst)
                
            if dpminc!=minc: 
                print "Error urOpt Cost: ",dpminc,minc

            if cnt!=len(minst) or dpminc!=minc: 
                sys.exit(-1) # report err

costtypedict={}


def usage():
    print """

    
Usage: 
Input options:
  -s FILE/TREE - define species tree(s); optional, used for testing correctness
  -g FILE/TREE - define gene tree(s)    

Reconciliation:  
  -c D|L|M|C|2 - cost spec: D=Dup (default), L=Loss, M=Dup+Loss, C=Deep Coalescence, 2=DC-classic
  -e - smp reconciliation (cost computation naive vs. star/cluster based)

2. Typical run for unrooted gene tree input; Only dynamic programming (DP); Dup cost (default). 
The script print the cost summary and the list of the species trees with the minimal cost.

./urexact.py  -g gtrees.txt
Min urD cost (DP): 0
(((a,c),b),d)
(((a,b),c),d)

3. Unrooted Loss Cost by DP.

./urexact.py -cL -g gtrees.txt
Min urL cost (DP): 1
(a,(b,(c,d)))
((a,b),(c,d))
(((a,c),b),d)
((a,(c,d)),b)
(((a,b),d),c)

4. Testing correctness of DP by providing the set of all rooted species trees over N leaves (e.g., 3.txt, 4.txt. etc.)

./urexact.py -g '(a,(b,(c,c)))' -s 3.txt

Min D cost (naive):  1
(c,(a,b))
(b,(a,c))
(a,(b,c))
Min urD cost (DP): 1
(a,(b,c))
((a,c),b)
((a,b),c)


5. Testing correctness of unrooted cost computation (-e): c(G,S) formula vs naive method
urexact.py -cL -e -g "(((c,a),(a,c)),(a,d))" -s 4.txt

Testing correcntess: G vs fixed S
G=(((c,a),(a,c)),(a,d)) S=(d,(c,(a,b)))  L=4 OK
G=(((c,a),(a,c)),(a,d)) S=(d,(b,(a,c)))  L=2 OK
G=(((c,a),(a,c)),(a,d)) S=(d,(a,(b,c)))  L=3 OK
G=(((c,a),(a,c)),(a,d)) S=(a,(d,(b,c)))  L=5 OK
G=(((c,a),(a,c)),(a,d)) S=(b,(d,(a,c)))  L=1 OK
G=(((c,a),(a,c)),(a,d)) S=(c,(d,(a,b)))  L=6 OK
G=(((c,a),(a,c)),(a,d)) S=(c,(b,(a,d)))  L=6 OK
G=(((c,a),(a,c)),(a,d)) S=(c,(a,(b,d)))  L=4 OK
G=(((c,a),(a,c)),(a,d)) S=((a,c),(b,d))  L=2 OK
G=(((c,a),(a,c)),(a,d)) S=(a,(c,(b,d)))  L=4 OK
G=(((c,a),(a,c)),(a,d)) S=((b,c),(a,d))  L=5 OK
G=(((c,a),(a,c)),(a,d)) S=(b,(c,(a,d)))  L=3 OK
G=(((c,a),(a,c)),(a,d)) S=(b,(a,(c,d)))  L=3 OK
G=(((c,a),(a,c)),(a,d)) S=((a,b),(c,d))  L=6 OK
G=(((c,a),(a,c)),(a,d)) S=(a,(b,(c,d)))  L=6 OK


6. The contents of the example file:

cat gtrees.txt
(a,((b,b),c))
((a,b),(c,d)) 
"""

def printargs():
    print sys.argv[0],
    for i in sys.argv[1:]:
        if i[0]=='-' : print i,
        else: print "\"%s\""%i,
    print 
    
def main():

    try:
        opts, args = getopt.getopt(sys.argv[1:], "s:g:v:p:m:c:r:e", ["help", "output="])
    except getopt.GetoptError as err:
        print str(err) 
        usage()
        sys.exit(2)

    if len(sys.argv)==1:
        usage()        
        sys.exit(1)
    
    stree = None
    gtrees = []
    gt=st=None
    global verbose,delnodes

    # initialize cost 
    for c in  [ CostDup, CostDC, CostDL, CostL, CostDC2  ]: 
        o=c()
        costtypedict[o.optname()]=o

    outputfile=None
    runmer=1 # default run
    gtreefile=None
    firstgtrees=-1
    numtrees=-1
    
    delnodes=""
    
    st=[]
    runopt=OPTDP
        
 
    costtype='D'
    for o, a in opts:
        if o == "-s": st.extend(ftread(a))        
        elif o == "-v": verbose=str(a)        
        elif o == "-p":
            gtree,stree=a.strip().split(" ")
            gtrees.append(Tree(str2tree(gtree)))
            st.append(Tree(str2tree(stree)))
        elif o == "-r":
            n=int(a)
            
            gtrees.append(Tree(randtree(n)))
            st.append(Tree(randtree(n)))
            
        elif o == "-g": gtrees.extend(ftread(a))            
        elif o == "-m": gtreefile=a 
        elif o == "-c": costtype=a                       
        elif o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o =='-u': unrooted=1
        elif o =='-e': runopt=OPTSINGLE
        else:
            assert False, "unhandled option"    

    cost=costtypedict[costtype]
    
    for l in args:
        gtrees,st=readintervalfile(l)


    if 'g' in verbose: 
        for g in gtrees: 
            print(g)

    if not st and not gtrees:
        print "Some trees have to be defined"
        usage()
        sys.exit(3)
    
    
    processunrootedtrees(gtrees,st,cost,runopt)        
    
        
    return 0
        

if __name__ == "__main__":
    main()
