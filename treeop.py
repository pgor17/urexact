import sys
import random

def leaf(s): return type(s)==str
def comparable(a,b): set(a).intersection(set(b))
def num(c): return ordext(c)
def ns(*s): return " ".join("".join(i) for i in s)

def height(t):
    if leaf(t): return 0
    return 1+max(height(t[0]),height(t[1]))
           
def theight(t,l):

    def _th(t,l,h):
        if leaf(t):
            if l==t: return h
            return 0

        mx=_th(t[0],l,h+1)
        if mx>0: return mx
        return _th(t[1],l,h+1)

    return _th(t,l,0)

def startype(a,b,c,topssplit):
    def empty(x): return x.issubset(topssplit[0]) or x.issubset(topssplit[1]) 
    def dbl(x,cx): return not empty(x) and not empty(cx)

    ab=a.union(b)
    bc=b.union(c)
    ca=c.union(a)

    if not empty(a) and  empty(bc): return (1,a,b,c)
    if not empty(b) and  empty(ca): return (1,b,c,a)
    if not empty(c) and  empty(ab): return (1,c,a,b)
 
    #print "STAR",ns(a,b,c)
    #for x,y in ((a,bc),(b,ca),(c,bc)):
    #    print ns(x),empty(x),ns(y),empty(y)

    if empty(a) and empty(bc): return (2,a,b,c)
    if empty(b) and empty(ca): return (2,b,c,a)    
    if empty(c) and empty(ab): return (2,c,b,a)

    d=0
    if dbl(a,bc): d+=1
    if dbl(b,ca): d+=1
    if dbl(c,ab): d+=1


    if d==3: return (4,a,b,c)
    
    if d==2: 
        if not dbl(a,bc): return (5,a,b,c)
        if not dbl(b,ca): return (5,b,c,a)
        if not dbl(c,ab): return (5,c,a,b)        

    if d==1: 
        if dbl(a,bc): return (3,a,b,c)
        if dbl(b,ca): return (3,b,c,a)
        if dbl(c,ab): return (3,c,a,b)

    # if not empty(a): return (1,a,b,c)
    # if not empty(b): return (1,b,c,a)
    # if not empty(c): return (1,c,b,a)

    raise Exception("Unknown star")
    return (0,a,b,c)

    


lfalf=[ chr(i) for i in xrange(ord('a'),ord('z')) ]+[ chr(i) for i in xrange(ord('A'),ord('Z')) ]
#lfalf="ABCDEF" #abcdyzghjiklmopqr123456"

lfalf=[ chr(i) for i in xrange(ord('a'),ord('z')) ]+[ chr(i) for i in xrange(ord('A'),ord('Z')) ]
def ordext(c): 
    return lfalf.index(c)


# krotka -> napis
def pt(s): return str(s).replace("'",'').replace(" ",'')

# napis -> krotka
def str2tree(s):

    def _st(s,p):
        if not s:
            raise Exception("String too short: <%s>"%s)
        if s[p]=='(':
            t,p=_st(s,p+1)
            if s[p]!=',': raise Exception("comma expected")
            t2,p=_st(s,p+1)
            if s[p]!=')': raise Exception(") expected")
            return ((t,t2),p+1)
        r=''
        while s[p].isalnum():
            r+=s[p]
            p=p+1
        return (r,p)

    if not s.strip():
        print "Warning: empty string"
        return []
    return _st(s,0)[0]


def node2label(n):
    return str(n).replace("("," ").replace(")"," ").replace(","," ")


lfalf=[ chr(i) for i in xrange(ord('a'),ord('z')) ]+[ chr(i) for i in xrange(ord('A'),ord('Z')) ]


def getclusters(s):
    def _gc(s,d):
        d[s]=cluster(s)
        if not leaf(s):
            _gc(s[0],d)
            _gc(s[1],d)
    d={}
    _gc(s,d)
    return d

class Node:
    def __init__(self, tup, par):
        self.src=tup            
        self.parent=par

        if self.parent:
            self.depth=self.parent.depth+1
        else: 
            self.depth=0


        if self.parent:
            self.height=self.parent.height+1
        else: self.height=0
        self.stheight=height(tup)
        if self.leaf():
            self.cluster=frozenset([tup])
            self.clusterleaf=tup
        else:
            self.l=Node(tup[0],self)
            self.r=Node(tup[1],self)
            self.cluster=self.r.cluster | self.l.cluster
            self.clusterleaf=None

        self.interval=None

    def leaf(self):
        return type(self.src)==str

    def setinterval(self,s,distornode):
        if type(distornode)==int:
            self.interval=[s,s.ancestor(distornode)]
        else: self.interval=[s,distornode]

    def ancestor(self,dist):
        if dist==0 or not self.parent: return self
        return self.parent.ancestor(dist-1)

    def __str__(self):
        if self.leaf(): return self.clusterleaf
        return "("+self.l.__str__()+","+self.r.__str__()+")"

    def __repr__(self):
        return self.__str__()


    def nodes(self): # postorder
        if self.leaf(): return [ self ]
        return  self.l.nodes()+ self.r.nodes() + [self]


    def leaves(self):
        if self.leaf(): return [ self ]
        return  self.l.leaves()+ self.r.leaves()
    
    def sibling(self):
        if not self.parent: return None
        if self.parent.l==self: return self.parent.r
        return self.parent.l
    
   # x = self or x below sel.
    def geq(self,x):
        while x:
            if x==self: return True
            x=x.parent
        return False

    def leq(self,x):
        return x.geq(self)

    def comparable(self,x):
        return self.geq(x) or self.leq(x)

    def lca(self,y):
        a,b=self,y
        if a.height>b.height: a,b=b,a
        # a.h <= b.h        
        while b.height!=a.height: b=b.parent

        # a.h == b.h        
        while True:
            if a==b: return a
            a=a.parent
            b=b.parent

    def findnode(self,cluster):
        #print "FD",self,cluster,self.src
        if self.cluster==cluster: return self
        if self.leaf(): return None
        r=self.l.findnode(cluster)
        if r: return r
        return self.r.findnode(cluster)

    def _allrootings(self,top):  
        if not top: 
            return self.r._allrootings(self.l.src)+self.l._allrootings(self.r.src)
        l=[ (self.src,top) ]                    
        if not self.leaf():            
            return l+self.r._allrootings((self.l.src,top))+self.l._allrootings((self.r.src,top))
        return l

    def upcluster(self):
        n=self
        c=frozenset()
        while n and n.sibling():
            c=c.union(n.sibling().cluster)
            n=n.parent            
        return c

    def star(self):
        return self.l.cluster,self.r.cluster,self.upcluster()

    def splits(self):
        return [ (g.l.cluster,g.r.cluster) for g in self.nodes() if not g.leaf() ]


class Tree:
    def __init__(self,tup):
        self.srclist=None
        #print type(tup),tup
        if type(tup)==list:
            self.srclist=tup
            tup=self.srclist[0]
            for t in self.srclist[1:]: tup=(tup,t)

        self.root=Node(tup,None)
        self.nodes=self.root.nodes()
        for i,n in enumerate(self.nodes): 
            n.num=i
            n.artificial=False
        self.src=tup

        if self.srclist:
            l=self.root
            for i in xrange(len(self.srclist)-1):
                #print "!",l
                l.artificial=True
                l=l.l
        
    def leaves(self):
        return self.root.leaves()

    def allrootings(self): 
        return self.root._allrootings(None)
        

    def dccost(self,stree):   
        self.setlcamapping(stree)     
        c=0
        for n in self.nodes:            
            if n<>self.root: 
                c+=n.lcamap.depth-n.parent.lcamap.depth-1
        return c
    def splits(self): return self.root.splits()

    def dccost2(self,stree):        
        self.setlcamapping(stree)
        c=0
        for n in self.nodes:            
            if n<>self.root: 
                c+=n.lcamap.depth-n.parent.lcamap.depth
        return c


    def dupcost(self,stree):        
        c=0
        self.setlcamapping(stree)
        for n in self.nodes:            
            if n.leaf(): continue
            if n.l.lcamap==n.lcamap or n.r.lcamap==n.lcamap: c+=1
        return c

    # Computing urcosts
    def urmincost(self,costcomp):
        lst=[ (costcomp(Tree(g)),g) for g in self.allrootings() ]
        mc=min(c for c,_ in lst)
        for c,mt in lst: 
            if c==mc: return mt,mc
    

    

    # Naive by rooted cases
    def ugencostnaive(self,stree,cost):
        mt=None
        mc=0
        for g in self.allrootings():    
            cc=Tree(g).gencost(stree,cost)            
            if not mt or mc>=cc:
                mt=g
                mc=cc
        return mt,mc
    
    def stars(self):
        return [ n.star() for n in self.nodes if not n.leaf() and n!=self.root ]

    def ugencost(self,stree,cost,verbose=''):
        #print "UGE"
        sleaves=stree.root.cluster        
        croot=stree.root                
        while True:                                
            if self.root.cluster.issubset(croot.l.cluster): croot=croot.l
            elif self.root.cluster.issubset(croot.r.cluster): croot=croot.r
            else: break
        ssplits=croot.splits()                      
        topssplit=(croot.l.cluster,croot.r.cluster)
        #print "TOPSPLIT",croot,topssplit

        cst=sum(cost.ugammaf(a,b,c,x,y,topssplit) for a,b,c in self.stars() for x,y in ssplits) 
        #print cst
        cst+=sum(cost.ulambdaf(a,b,c,x,topssplit) for a,b,c in self.stars() for x in sleaves) 

        if 'u' in verbose:
            print "In ugencost",pt(self)
            for a,b,c in self.stars():
                star,e1,e2,e3=startype(a,b,c,topssplit)
                print "%s.%s.%s S%d"%(ns(e1),ns(e2),ns(e3),star)
                for x,y in ssplits:        
                    z=cost.ugammaf(a,b,c,x,y,topssplit)
                    
                    if x==topssplit[0] or x==topssplit[1]:
                        print "   TOP=", # %s^%s ->> %g"%(ns(topssplit[0]),ns(topssplit[1]),z)    
                    else:
                        print "       ",
                    print "%s|%s %s^%s ->> %g"%(ns(x),ns(y),ns(topssplit[0]),ns(topssplit[1]),z)

                for x in sleaves:
                    z=cost.ulambdaf(a,b,c,x,topssplit)
                    if z:
                        print "   LF=%s %s^%s ->> %d"%(ns(x),ns(topssplit[0]),ns(topssplit[1]),z)

        return cst+cost.urtreecorrection([self])
        


    def gencost(self,stree,cost,verbose=''):
        sleaves=stree.root.cluster
        c=0
        gsplits=self.splits()
        ssplits=stree.splits() #!
        #print sleaves,gsplits

        c=sum(cost.lambdaf(a,b,frozenset(s)) for s in sleaves for a,b in gsplits) 

        if 'r' in verbose:
            print "RGenCost",pt(self)
            for s in sleaves:
                for a,b in gsplits:
                    z=float(cost.lambdaf(a,b,frozenset(s)))
                    if z: print "   LF=%s %s|%s %g"%(s,ns(a),ns(b),z)

            # i=sum(gammaf(a,b,x,y) for a,b in gsplits for x,y in ssplits ) 
        i=0
        # print 
        for a,b in gsplits:            
            for x,y in ssplits:
                z=cost.gammaf(a,b,x,y)
                i+=z
                if z and 'r' in verbose: print "   %s.%s %s|%s"%(ns(a),ns(b),ns(x),ns(y)),z
        return c+i+cost.rtreecorrection([self])


    def duplosscost(self,tree):
        return self.dccost(tree)+3*self.dupcost(tree)

    def losscost(self,tree):
        return self.dccost(tree)+2*self.dupcost(tree)        

    def findnodeplus(self,cluster):

        if len(cluster)>2 and cluster[-2]=="+":
            up=int(cluster[-1])
            cluster=cluster[:-2]
        elif len(cluster)>1 and cluster[-1][0]=="+":
            up=int(cluster[-1])
            cluster=cluster[:-1]
        else:
            up=0

        s=self.root.findnode(frozenset(cluster))
        if not s: return s

        while up and s.parent:
            s=s.parent
            up=up-1
        return s

    def __str__(self):
        return self.root.__str__()

  
    def setlcamapping(self,st):
        for l in self.leaves():
            clu=l.clusterleaf
            l.lcamap=None
            while clu and not l.lcamap:
                l.lcamap=st.root.findnode(frozenset([clu]))
                clu=clu[:-1]
            if not l.lcamap:
                raise Exception("Lca mapping not found for ",l)

        for l in self.nodes:
            if not l.leaf():
                l.lcamap=l.r.lcamap.lca(l.l.lcamap)
            #print "LCA",l,l.lcamap

        
    def __repr__(self):
        return self.root.__repr__()

    
    def lcacluster(self,cluster):
        c=set(cluster)
        for n in self.nodes:
            if c.issubset(set(n.cluster)): return n
     
    
    def clusters(self):
        return set(n.cluster for n in self.nodes)


    def height(self):
        return self.root.stheight



        
def randtreecf(c,f):
    return randtree(c*2+f,c)


def randtree(n,chnum=0):
    l=lfalf[:]
    c=0
    while n>len(l):
        l.extend(lab+"%d"%c for lab in lfalf)
        c+=1
    #print "_tr",n,chnum
    return _randtree(l[:n],chnum)
           
    
def _randtree(lst,chnum=0,balanced=0):
    if chnum<=0:
        if len(lst)==1: return lst[0]
        dp=random.randint(1,len(lst)-1)
        if dp>len(lst)/2:
            return (_randtree(lst[:dp]),_randtree(lst[dp:]))
        return (_randtree(lst[dp:]),_randtree(lst[:dp]))
        

    lf=lst[:]
    curch=chnum
    while len(lf)>1:    
        #print lf,curch,chnum
        curl1=lf.pop(random.randint(0,len(lf)-1))
        curl2=lf.pop(random.randint(0,len(lf)-1))
        
        if type(curl2)==str:
            curl1,curl2=curl2,curl1

        if curch>0:
            if type(curl1)==str and type(curl2)==str:
                lf.append((curl1,curl2))                
                curch-=1
                continue
        else:        
            if type(curl1)!=str or type(curl2)!=str:
                lf.append((curl1,curl2))
                continue
            
        lf.append(curl1)
        lf.append(curl2)


    return lf[0]
